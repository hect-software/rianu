# Rianú 

This project is called `rianú`, the Irish Language word for tracking. 

Rianú is an open-source software, released under the BSD 3-Clause License, designed to more efficiently track engineered cardiac tissues. For further information on installation, setup, and customization please visit [rianu.mrph.dev](https://rianu.mrph.dev).

[![Website](https://img.shields.io/website?down_color=red&down_message=offline&label=docs&style=flat&up_color=success&up_message=online&url=https://rianu.mrph.dev)](https://rianu.mrph.dev)
[![paper doi](https://img.shields.io/badge/paper%20doi-10.1016/j.cmpbup.2023.100107-blue)](https://doi.org/10.1016/j.cmpbup.2023.100107)
[![project doi](https://img.shields.io/badge/project%20doi-10.17605/OSF.IO/YWCHZ-blue)](https://doi.org/10.17605/OSF.IO/YWCHZ)
[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://gitlab.com/hect-software/rianu/-/blob/main/LICENSE)
[![pylint](https://hect-software.gitlab.io/rianu/badges/pylint.svg)](https://hect-software.gitlab.io/rianu/lint/)
[![code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pipeline status](https://gitlab.com/hect-software/rianu/badges/main/pipeline.svg)](https://gitlab.com/hect-software/rianu/commits/main)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)




## Pronunciation  
![](https://storageapi.fleek.co/jack-alope-team-bucket/Rianú.mp3)
_Pronunciation kindly supplied by a native Corkonian._

## Citation  

If you use this software in research we kindly ask that you cite it as:  
```bibtex
@article{MurphyRianu2023,
	doi = {10.1016/j.cmpbup.2023.100107},
	year = 2023,
	month = {5},
	publisher = {Elsevier {BV}},
	author = {Jack F. Murphy and Kevin D. Costa and Irene C. Turnbull},
	title = {Rian{\'{u}}: Multi-tissue tracking software for increased throughput of engineered cardiac tissue screening},
	journal = {Computer Methods and Programs in Biomedicine Update}
}

```  
## Contributing  
  
If you are a member of a lab with a different bioreactor setup or have different analysis needs and would like to contribute to this repository please see [`contributing.md`](https://gitlab.com/hect-software/rianu/-/blob/main/CONTRIBUTING.md) for instruction on how to setup for development. Alternatively, open and issue on this repository or email `jack@mrph.dev` and I can work to add support for your system. We are very open to collaboration and want this software to be applicable to as many systems as possible.

---

